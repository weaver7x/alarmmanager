package com.example.alarmexample;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public final class AlarmController {

	private final static byte PENDING_INTENT_ID = 0;

	public static PendingIntent createPendingIntent(final Context context) {
		return PendingIntent.getBroadcast(context, PENDING_INTENT_ID,
				new Intent(context, AlarmReceiver.class).putExtra("HOLA", true), 0);
	}

	public static void cancelPendingIntent(final Context context,
			final PendingIntent pendingIntent) {
		try {
			final AlarmManager manager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			if (manager != null) {
				manager.cancel(pendingIntent);
				Toast.makeText(context, "Alarma cancelada", Toast.LENGTH_SHORT)
						.show();
			}
		} catch (final Exception ex) {
			Log.d("CREADOR", "Hubo un error al cancelar.");
			ex.printStackTrace();
		}
	}

}
