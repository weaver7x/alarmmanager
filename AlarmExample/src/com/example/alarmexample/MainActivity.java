package com.example.alarmexample;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	private PendingIntent pendingIntent;
	private AlarmManager manager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Retrieve a PendingIntent that will perform a broadcast
		this.pendingIntent = AlarmController.createPendingIntent(this);
	}

	public void startAlarm(View view) {
		this.manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		int interval = 10000; // 10 seconds

		// Once set, the alarm will fire every 10 seconds (y no despierta al
		// dispositivo si esta apagado, para ello poner WAKEUP).
		this.manager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(),
				interval, this.pendingIntent);
		Toast.makeText(this, "Alarma establecida", Toast.LENGTH_SHORT).show();
	}

	public void cancelAlarm(View view) {
		// Opci�n 1
		// if (this.manager != null) {
		// this.manager.cancel(this.pendingIntent);
		// Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
		// }
		// Opci�n 2:
		this.pendingIntent = AlarmController.createPendingIntent(this);
		AlarmController.cancelPendingIntent(this, this.pendingIntent);
	}
}
