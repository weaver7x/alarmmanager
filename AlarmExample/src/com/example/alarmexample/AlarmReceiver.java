package com.example.alarmexample;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Seg�n las normas de Google, s�lo funciona el BOOT_COMPLETED si se ha abierto al menos 1 vez la aplicaci�n.
 * @author Cristian TG
 *
 */
public class AlarmReceiver extends BroadcastReceiver {
	public boolean isOnline(Context context) {

	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    return (netInfo != null && netInfo.isConnected());

	}
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		Log.d("CREADOR","Acci�n: "+arg1.getAction());
		
		 if (arg1.getAction()!=null&&arg1.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
	            // Set the alarm here.
			 Toast.makeText(
						arg0,
						"Despu�s del boot", Toast.LENGTH_LONG).show();
			 Log.d("CREADOR","Despu�s del boot");
	        }
		 else{
			 if (arg1.getAction()!=null&&arg1.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){
				 Log.d("CREADOR","Cambia el estado: "+isOnline(arg0));	 
			 }else{
				 final Boolean vieneDeAlarma=arg1.getBooleanExtra("HOLA", false);
				 if(vieneDeAlarma){
					 Log.d("CREADOR","Viene de alarma");	 
				 }else{
				 Log.d("CREADOR","Llamada alarma normal");
				 }
			 }
			 
		 }
		 
		 
		// For our recurring task, we'll just display a message
		// Se borra del dispositivo una vez notificada
		final long DEFECTO = 0;
		final String CLAVE_GUARDAR = "CLAVE_GUARDAR";

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(arg0);
		final long contadorActual = prefs.getLong(CLAVE_GUARDAR, DEFECTO);
		final SharedPreferences.Editor editor = prefs.edit();

		long nuevoValor = contadorActual + 1;
		if (contadorActual > 10) {
			Log.d("CREADOR",
					"Es mayor que 10, lo pongo a 0 y cancelo la alarma.");
			final PendingIntent pendingIntent = AlarmController
					.createPendingIntent(arg0);
			// PendingIntent.getBroadcast(arg0, MainActivity.PENDING_INTENT_ID,
			// arg1, 0);
			nuevoValor = 0;
			AlarmController.cancelPendingIntent(arg0, pendingIntent);
		}

		try {
			editor.putLong(CLAVE_GUARDAR, nuevoValor);
		} catch (final Exception e) {
		} finally {
			editor.commit();
		}

		Toast.makeText(
				arg0,
				"Antes: " + contadorActual + " Despu�s: "
						+ nuevoValor, Toast.LENGTH_LONG).show();

	}
}
